export default [
  {
    path: '/demo/tree-filter',
    name: 'treeFilter',
    component: () => import('@/views/demo/test')
  },
  {
    path: '/demo/bread',
    name: 'bread',
    component: () => import('@/views/demo/append-bread-crumb')
  },
  {
    path: '/demo/other',
    name: 'other',
    component: () => import('@/views/demo/go-other')
  },
  {
    path: '/demo/detail',
    name: 'detail',
    component: () => import('@/views/demo/detail')
  },
  {
    path: '/demo/search-page',
    name: 'search-page',
    component: () => import('@/views/demo/search-page')
  }, {
    path: '/demo/city-select',
    name: 'citySelect',
    component: () => import('@/views/demo/city-select')
  }
]
