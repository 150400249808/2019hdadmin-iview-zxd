export default [
  {
    path: '/platform/cars',
    name: 'platformCars',
    meta: {
      title: '车辆管理'
    },
    component: () => import('@/views/pages/platform/cars')
  }, {
    path: '/platform/company',
    name: 'platformCompany',
    meta: {
      title: '企业管理'
    },
    component: () => import('@/views/pages/platform/company')
  }, {
    path: '/platform/equipment',
    name: 'platformEquipment',
    meta: {
      title: '设备管理'
    },
    component: () => import('@/views/pages/platform/equipment')
  }, {
    path: '/platform/staff',
    name: 'platformStaff',
    meta: {
      title: '员工管理'
    },
    component: () => import('@/views/pages/platform/staff')
  }, {
    path: '/system/users',
    name: 'systemUsers',
    meta: {
      title: '用户管理'
    },
    component: () => import('@/views/pages/system/users')
  }, {
    path: '/system/role',
    name: 'systemRole',
    meta: {
      title: '角色权限管理'
    },
    component: () => import('@/views/pages/system/role')
  }, {
    path: '/system/cars',
    name: 'systemCars',
    meta: {
      title: '用户车辆管理'
    },
    component: () => import('@/views/pages/system/cars')
  }, {
    path: '/system/dict',
    name: 'systemDict',
    meta: {
      title: '字典查询'
    },
    component: () => import('@/views/pages/system/dict')
  }, {
    path: '/system/log',
    name: 'systemLog',
    meta: {
      title: '日志管理'
    },
    component: () => import('@/views/pages/system/log')
  }, {
    path: '/statement/cars',
    name: 'statementCars',
    meta: {
      title: '车辆报表'
    },
    component: () => import('@/views/pages/statement/cars')
  }, {
    path: '/statement/line',
    name: 'statementLine',
    meta: {
      title: '在线报表'
    },
    component: () => import('@/views/pages/statement/line')
  }, {
    path: '/insurance/yu-jing',
    name: 'insuranceYuJing',
    meta: {
      title: '实时预警'
    },
    component: () => import('@/views/pages/insurance/yu-jing')
  }, {
    path: '/insurance/shai-cha',
    name: 'insuranceShaiCha',
    meta: {
      title: '风险筛查'
    },
    component: () => import('@/views/pages/insurance/shai-cha')
  }, {
    path: '/insurance/hua-xiang',
    name: 'insuranceHuaXiang',
    meta: {
      title: '风险画像'
    },
    component: () => import('@/views/pages/insurance/hua-xiang')
  }, {
    path: '/insurance/guan-li',
    name: 'insuranceGuanLi',
    meta: {
      title: '风险管理'
    },
    component: () => import('@/views/pages/insurance/guan-li')
  }, {
    path: '/insurance/ping-gu',
    name: 'insurancePingGu',
    meta: {
      title: '风险评估'
    },
    component: () => import('@/views/pages/insurance/ping-gu')
  }, {
    path: '/insurance/bao-gao',
    name: 'insuranceBaoGao',
    meta: {
      title: '评估报告'
    },
    component: () => import('@/views/pages/insurance/bao-gao')
  }, {
    path: '/map/position',
    name: 'mapPosition',
    meta: {
      title: '定位'
    },
    component: () => import('@/views/pages/map/position')
  }
]
