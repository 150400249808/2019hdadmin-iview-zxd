import Home from '@/views/pages/home'
import demoRouter from './group/demo'
import platformRouter from './group/platform'

const base = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '/index',
        name: 'index',
        component: () => import('@/views/pages/index')
      },
      {
        path: '/iframe',
        name: 'iframe',
        component: () => import('@/views/pages/iframe')
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/pages/login')
  }
]
base[0].children.push.apply(base[0].children, demoRouter)
base[0].children.push.apply(base[0].children, platformRouter)
export default base
