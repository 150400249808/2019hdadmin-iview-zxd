import axios from 'axios'
import qs from 'qs'
import store from '../store'
import md5 from 'js-md5'
import moment from 'moment'

const addErrorLog = errorInfo => {
  // TODO 对错误日志进行收集
}

class HttpRequest {
  constructor (baseUrl = baseURL) {
    this.baseUrl = baseUrl
    this.queue = {}
  }
  getInsideConfig () {
    const config = {
      baseURL: this.baseUrl,
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }
    }
    return config
  }
  destroy (url) {
    delete this.queue[url]
    if (!Object.keys(this.queue).length) {
      // TODO页面取消正在加载的特效
    }
  }
  interceptors (instance, url) {
    // 请求拦截
    instance.interceptors.request.use(config => {
      // 添加全局的loading...
      if (!Object.keys(this.queue).length) {
        // TODO根据不同框架添加不同的加载特效
      }
      this.queue[url] = true
      return config
    }, error => {
      return Promise.reject(error)
    })
    // 响应拦截
    instance.interceptors.response.use(res => {
      this.destroy(url)
      const { data, status } = res
      if (data.errcode === 1001) {
        // TODO根据不同的返回结果进行全局拦截
      }
      return { data, status }
    }, error => {
      this.destroy(url)
      let errorInfo = error.response
      if (!errorInfo) {
        const { request: { statusText, status }, config } = JSON.parse(JSON.stringify(error))
        errorInfo = {
          statusText,
          status,
          request: { responseURL: config.url }
        }
      }
      addErrorLog(errorInfo)
      return Promise.reject(error)
    })
  }
  request (options) {
    let timestamp = moment.utc().valueOf()
    let token = store.state.user.token ? store.state.user.token : ''
    let sign = md5(token + timestamp).toUpperCase()
    let userid = store.state.user.severUserInfo.setting ? store.state.user.severUserInfo.setting.user_id : ''
    options.data.sign = sign
    options.data.timestamp = timestamp
    options.data.userid = userid
    const instance = axios.create()
    if (options.type === 'get') {
      options.method = 'get'
      options.params = options.data
    } else {
      options.method = 'post'
    }
    delete options.type
    if (options.php === true) {
      options.headers = { 'Content-Type': 'application/x-www-form-urlencoded' }
      options.data = qs.stringify(options.data)
    }
    options = Object.assign(this.getInsideConfig(), options)

    this.interceptors(instance, options.url)
    if (options.await === true) {
      return new Promise((resolve, reject) => {
        instance(options)
          .then((response) => {
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    } else {
      return instance(options)
    }
  }
}
export default HttpRequest
