import { getExplorer, toTree, treeFind, getTreePedigree } from '@/libs/tools'
import moment from 'moment'
import md5 from 'js-md5'
import xeUtils from 'xe-utils'
export default {
  getExplorer: function () {
    return getExplorer()
  },
  moment,
  md5,
  toTree,
  treeFind,
  getTreePedigree,
  xeUtils
}
