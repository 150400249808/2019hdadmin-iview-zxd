import redCar from '@/assets/images/cars/red.png'
import blueCar from '@/assets/images/cars/blue.png'
import yellowCar from '@/assets/images/cars/yellow.png'
import greenCar from '@/assets/images/cars/green.png'
export default [
  {
    position: {
      lng: 123.445224,
      lat: 41.696974
    },
    icon: { url: redCar, size: { width: 48, height: 48 } }
  }, {
    position: {
      lng: 123.408429,
      lat: 41.698697
    },
    icon: { url: blueCar, size: { width: 48, height: 48 } }
  }, {
    position: {
      lng: 123.438429,
      lat: 41.693697
    },
    icon: { url: yellowCar, size: { width: 48, height: 48 } }
  }, {
    position: {
      lng: 123.418429,
      lat: 41.695697
    },
    icon: { url: greenCar, size: { width: 48, height: 48 } }
  }
]
