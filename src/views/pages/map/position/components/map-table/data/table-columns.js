export default {
  columns1: [
    {
      title: '车牌号',
      minWidth: 30,
      slot: 'action2'
    },
    {
      title: '在线状态',
      minWidth: 30,
      key: 'online'
    }, {
      title: '设备号',
      minWidth: 30,
      key: 'device_id'
    }, {
      title: '所属单位',
      minWidth: 40,
      key: 'unit_name'
    }, {
      title: 'GPS时间',
      minWidth: 40,
      key: 'gps_time'
    }, {
      title: 'GPS速度',
      minWidth: 20,
      key: 'speed'
    }, {
      title: '行车速度',
      minWidth: 20,
      key: 'rec_speed'
    }, {
      title: '高度',
      minWidth: 10,
      key: 'height'
    }, {
      title: '方向',
      minWidth: 10,
      key: 'direct'
    }, {
      title: '定位状态',
      minWidth: 30,
      key: ''
    }, {
      title: '状态描述',
      minWidth: 60,
      key: 'state_flag'
    }, {
      title: '位置',
      slot: 'action',
      minWidth: 60
    }
  ],
  columns2: [
    {
      title: '车牌号',
      slot: 'action1'
    },
    {
      title: '所属单位',
      key: 'unit_name'
    }, {
      title: 'GPS时间',
      key: 'gps_time'
    }, {
      title: 'GPS速度',
      key: 'speed'
    }, {
      title: '在线状态',
      key: 'online'
    }, {
      title: '高度',
      key: 'height'
    }, {
      title: '方向',
      key: 'direct'
    }, {
      title: '定位状态',
      key: 'a8'
    }, {
      title: '状态描述',
      key: 'state_flag'
    }, {
      title: '位置',
      slot: 'action'
    }
  ],
  columns3: [
    {
      title: '车牌号',
      key: 'a1'
    },
    {
      title: '终端号',
      key: 'a2'
    }, {
      title: '指令ID',
      key: 'a3'
    }, {
      title: '发送状态',
      key: 'a4'
    }, {
      title: '发送时间',
      key: 'a5'
    }, {
      title: '更新时间',
      key: 'a6'
    }
  ]
}
