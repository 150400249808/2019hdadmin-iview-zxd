import { getHomeRoute, getBreadCrumbList } from '@/libs/util'
import { getJavaMenus } from '@/api/user'
// import localMenusData from '@/mock/data/menus-data'
import config from '@/config'
import common from '@/libs/common'

const { homeName } = config

export default {
  state: {
    breadCrumbList: [],
    accessApiData: [],
    accessButton: '',
    menuListData: [],
    homeRoute: {},
    menuSelectId: 0,
    treeSelectData: {
      id: '',
      checkData: []
    },
    treeOptions: {
      multiple: false,
      showCheckbox: false
    },
    iframeUrl: '',
    cityData: [],
    cityOriginData: []
  },
  getters: {
    getBreadCrumbList: state => state.breadCrumbList
  },
  mutations: {
    setBreadCrumb (state, data) {
      state.breadCrumbList = data
    },
    setAccessApiData (state, data) {
      state.accessApiData = data
    },
    setBccessButton (state, data) {
      state.accessButton = data
    },
    setIframeUrl (state, data) {
      state.iframeUrl = data
    },
    setCityData (state, data) {
      state.cityData = data
    },
    setCityOriginData (state, data) {
      state.cityOriginData = data
    },
    setMenuSelectId (state, data) {
      state.menuSelectId = data
    },
    setTreeOptions (state, data) {
      state.treeOptions = data
    },
    setTreeSelectData (state, data) {
      state.treeSelectData = Object.assign({}, state.treeSelectData, data)
    },
    appendBreadCrumb (state, data) {
      state.breadCrumbList = data
    },
    setMenuListData (state, data) {
      state.menuListData = state.accessApiData[data].children
    },
    setHomeRoute (state, routes) {
      state.homeRoute = getHomeRoute(routes, homeName)
    }
  },
  actions: {
    getPhpMenusData ({ commit, rootState }, id) {
      commit('setMenuListData', id)
    },
    getJavaMenusData ({ commit, rootState }, id) {
      getJavaMenus(id).then((ret) => {
        let listData = javaMenusfilter(ret.data.data, rootState.user.access)
        commit('setMenuListData', listData)
      })
    },
    setBreadCrumb ({ commit, rootState }, obj) {
      let menuData = { title: '首页', icon: 'icon', path: '/', children: obj.list }
      let name = obj.name
      let is_key = 'id'
      if (name === '' || name === undefined || name === null) {
        name = obj.path
        is_key = 'path'
      }
      let BreadCrumbList = getBreadCrumbList(menuData, { path: obj.path, id: obj.id }, is_key)
      let count = BreadCrumbList.length
      BreadCrumbList.forEach((item, index) => {
        if (count !== (index + 1)) {
          item.path = ''
        }
      })
      if (menuData.length === 0) {
        commit('setBreadCrumb', [])
      } else if (BreadCrumbList.length > 0) {
        commit('setBreadCrumb', BreadCrumbList)
      }
    },
    appendBreadCrumb ({ commit, rootState }, obj) {
      let BreadCrumbList = rootState.app.breadCrumbList
      BreadCrumbList.push(obj)
      commit('appendBreadCrumb', BreadCrumbList)
    },
    setTreeData ({ commit, rootState }, obj) {
      commit('setTreeSelectData', obj)
    },
    setMenuSelectId ({ commit, rootState }, obj) {
      commit('setMenuSelectId', obj)
    },
    setTreeOptions ({ commit, rootState }, obj) {
      commit('setTreeOptions', obj)
    },
    setCityData ({ commit, rootState }, obj) {
      commit('setCityData', obj)
    },
    setIframeUrl ({ commit, rootState }, obj) {
      commit('setIframeUrl', obj)
    },
    setCityOriginData ({ commit, rootState }, obj) {
      commit('setCityOriginData', obj)
    },
    setAccessApiData ({ commit, rootState }, obj) {
      let access = ''
      let menusData = []
      obj.forEach(item => {
        if (item.permit__type === 0) {
          menusData.push({
            id: item.permit__id,
            pid: item.permit__pid,
            title: item.permit__name,
            path: item.permit__vue,
            url: item.permit__url,
            icon: item.permit__icon,
            type: item.permit__type,
            glass: item.permit__glass
          })
        } else {
          access += item.permit__vue + ','
        }
      })
      let menus = common.toTree(menusData, 'children', 'id', 'pid')
      console.log(obj, 'menus')
      commit('setBccessButton', access)
      commit('setAccessApiData', menus)
    }
  }
}
