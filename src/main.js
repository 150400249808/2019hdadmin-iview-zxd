import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import common from '@/libs/common'
import config from '@/config'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import 'vxe-table/lib/index.css'
import 'vxe-table-plugin-iview/dist/style.css'

import echarts from 'echarts'

import VueBaiDuMap from 'vue-baidu-map'

import VXETable from 'vxe-table'
import VXETablePluginIView from 'vxe-table-plugin-iview'

if (process.env.NODE_ENV !== 'production') require('@/mock')
Vue.use(iView)
Vue.use(VXETable)
VXETable.use(VXETablePluginIView)

Vue.config.productionTip = false
Vue.prototype.$common = common
Vue.prototype.$config = config
Vue.prototype.$echarts = echarts

Vue.use(VueBaiDuMap, { ak: 'OTkShjhBPkGgTb8gGfk5l73D' })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
