import axios from '@/libs/api.request'

export const unit = (data, type) => {
  if (type !== 'post') {
    type = 'get'
  }
  return axios.request({
    url: '/rest/unit/',
    data: data,
    type: type
  })
}
export const vehicle = (data) => {
  return axios.request({
    url: '/rest/vehicle',
    data: data,
    type: 'get',
    php: true
  })
}
export const sel_vehicle = (data) => {
  return axios.request({
    url: '/rest/vehicle/search',
    data: data,
    type: 'get',
    php: true
  })
}
export const postvehicle = (data) => {
  return axios.request({
    url: '/rest/vehicle/',
    data: data,
    type: 'post',
    php: true
  })
}
export const staff = (data) => {
  return axios.request({
    url: 'rest/people',
    data: data,
    type: 'get',
    php: true
  })
}
export const staffselect = (data) => {
  return axios.request({
    url: 'rest/people/search',
    data: data,
    type: 'get',
    php: true

  })
}
export const staffadd = (data) => {
  return axios.request({
    url: 'rest/people',
    data: data,
    type: 'post',
    php: true
  })
}
export const device = (data) => {
  return axios.request({
    url: 'rest/device',
    data: data,
    type: 'get',
    php: true
  })
}
export const deviceselect = (data) => {
  return axios.request({
    url: 'rest/device/search',
    data: data,
    type: 'get',
    php: true

  })
}
// 保存
export const deviceSave = (data) => {
  return axios.request({
    url: 'rest/device/',
    data: data,
    type: 'post'
    // php: true

  })
}
export const muser = (data) => {
  return axios.request({
    url: 'rest/muser',
    data: data,
    type: 'get',
    php: true
  })
}
export const muserselect = (data) => {
  return axios.request({
    url: 'rest/muser',
    data: data,
    type: 'get',
    php: true
  })
}
export const muserSave = (data) => {
  return axios.request({
    url: 'rest/muser/',
    data: data,
    type: 'post'
  })
}
export const updata = (data) => {
  return axios.request({
    url: 'rest/user/91/',
    data: data,
    type: 'put'
  })
}
export const dict = (data) => {
  return axios.request({
    url: 'rest/dict',
    data: data,
    type: 'get',
    php: true
  })
}

export const vehicleReport = (data) => {
  return axios.request({
    url: 'rest/vehicle/report',
    data: data,
    type: 'get',
    php: true
  })
}
export const vehicleSelect = (data) => {
  return axios.request({
    url: 'rest/vehicle/report',
    data: data,
    type: 'get',
    php: true
  })
}
export const exportData = (data) => {
  return axios.request({
    url: 'rest/vehicle/report',
    data: data,
    type: 'get',
    php: true
  })
}
