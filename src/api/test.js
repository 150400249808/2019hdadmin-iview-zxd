import axios from '@/libs/api.request'
export const vehicle = (data) => {
  return axios.request({
    url: '/rest/vehicle',
    data: data,
    type: 'get',
    php: true
  })
}
export const group = (data) => {
  return axios.request({
    url: '/rest/group',
    data: data,
    type: 'get',
    php: true
  })
}
export const op = (data) => {
  return axios.request({
    url: '/rest/op',
    data: data,
    type: 'get',
    php: true
  })
}
export const sle_op = (data) => {
  return axios.request({
    url: '/rest/op',
    data: data,
    type: 'get',
    php: true
  })
}
export const sell = (data) => {
  return axios.request({
    url: '/rest/counter/online',
    data: data,
    type: 'get',
    php: true
  })
}
export const uvehicle = (data) => {
  return axios.request({
    url: '/rest/uvehicle',
    data: data,
    type: 'get',
    php: true
  })
}
