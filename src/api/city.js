import axios from '@/libs/api.request'

/**
 * 指令状态接口
 * @param data
 * @returns {*|never|Promise<AxiosResponse<T>>|ClientHttp2Stream|Promise|Promise<any>|ClientRequest}
 */
export const getCityData = (data) => {
  return axios.request({
    url: 'rest/city',
    data: { search: '|210000|', size: 5000 },
    type: 'get',
    php: true
  })
}
