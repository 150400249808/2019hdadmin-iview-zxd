import axios from '@/libs/api.request'

/**
 * 指令状态接口
 * @param data
 * @returns {*|never|Promise<AxiosResponse<T>>|ClientHttp2Stream|Promise|Promise<any>|ClientRequest}
 */
export const cmdView = (data) => {
  return axios.request({
    url: 'rest/cmd/view',
    data: data,
    type: 'get',
    php: true
  })
}
/**
 * 车辆信息列表
 * @param data
 * @returns {*|Promise|Promise<any>|Window.Promise}
 */
export const restPos = (data) => {
  return axios.request({
    url: 'rest/pos',
    data: data,
    type: 'get',
    php: true
  })
}

export const restPosGeo = (data) => {
  return axios.request({
    url: 'rest/pos/geo',
    data: data,
    type: 'get',
    php: true
  })
}
