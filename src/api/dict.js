import axios from '@/libs/api.request'

/**
 * 指令状态接口
 * @param data
 * @returns {*|never|Promise<AxiosResponse<T>>|ClientHttp2Stream|Promise|Promise<any>|ClientRequest}
 */
export const getDictData = (data) => {
  return axios.request({
    url: 'rest/dict',
    data: data,
    type: 'get',
    php: true
  })
}
