import axios from '@/libs/api.request'

export const demo1 = (abc) => {
  return axios.request({
    url: '/Api/Api/tt',
    data: { id: abc },
    type: 'post',
    php: true
  })
}
export const demo2 = (abc) => {
  return axios.request({
    url: '/Api/Api/tt',
    data: { id: abc },
    type: 'post',
    php: true,
    await: true
  })
}

export const mock = (abc) => {
  return axios.request({
    url: '/wl/demo',
    data: { id: abc },
    type: 'post',
    php: true
  })
}
