export default [
  [],
  [
    {
      id: 1001,
      title: '地图概览',
      icon: 'ios-appstore',
      children: [
        { title: '位置检测', path: '/map/position', id: 100101, icon: 'ios-keypad' }
      ]
    }
  ],
  [],
  [],
  [
    {
      id: 3001,
      title: '基础报表',
      icon: 'ios-appstore',
      children: [
        { title: '车辆基础报表', path: '/statement/cars', id: 300101, icon: 'ios-keypad' },
        { title: '在线统计报表', path: '/statement/line', id: 300102, icon: 'ios-keypad' }
      ]
    }
  ],
  [
    {
      id: 2001,
      title: '用户管理',
      icon: 'ios-appstore',
      path: '/system/users'
    }, {
      id: 2002,
      title: '角色权限管理',
      icon: 'ios-appstore',
      path: '/system/role'
    }, {
      id: 2003,
      title: '用户车辆管理',
      icon: 'ios-appstore',
      path: '/system/cars'
    }, {
      id: 2004,
      title: '字典查询',
      icon: 'ios-appstore',
      path: '/system/dict'
    }, {
      id: 2005,
      title: '日志管理',
      icon: 'ios-appstore',
      path: '/system/log'
    }
  ],
  [
    {
      id: 1001,
      title: '运输企业管理',
      icon: 'ios-appstore',
      path: '/platform/company'
    }, {
      id: 1002,
      title: '车辆管理',
      icon: 'ios-appstore',
      path: '/platform/cars'
    }, {
      id: 1003,
      title: '人员管理',
      icon: 'ios-appstore',
      path: '/platform/staff'
    }, {
      id: 1004,
      title: '设备管理',
      icon: 'ios-appstore',
      path: '/platform/equipment'
    }
  ],
  [
    {
      id: 4001,
      title: '实时预警',
      icon: 'ios-appstore',
      path: '/insurance/yu-jing'
    },
    {
      id: 4002,
      title: '大数据分析',
      icon: 'ios-appstore',
      children: [
        { title: '风险筛查', path: '/insurance/shai-cha', id: 400101, icon: 'ios-keypad' },
        { title: '风险画像', path: '/insurance/hua-xiang', id: 400102, icon: 'ios-keypad' },
        { title: '风险管理', path: '/insurance/guan-li', id: 400103, icon: 'ios-keypad' },
        { title: '风险评估', path: '/insurance/ping-gu', id: 400104, icon: 'ios-keypad' },
        { title: '评估报告', path: '/insurance/bao-gao', id: 400105, icon: 'ios-keypad' }
      ]
    }
  ]
]
