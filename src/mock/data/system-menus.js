export default {
  errcode: 200,
  errmsg: 'ok',
  data: [
    {
      id: 1001,
      title: '运输企业管理',
      icon: 'ios-appstore',
      path: '/platform/company'
    }, {
      id: 1002,
      title: '车辆管理',
      icon: 'ios-appstore',
      path: '/platform/cars'
    }, {
      id: 1003,
      title: '人员管理',
      icon: 'ios-appstore',
      path: '/platform/staff'
    }, {
      id: 1004,
      title: '设备管理',
      icon: 'ios-appstore',
      path: '/platform/equipment'
    }
  ]
}
